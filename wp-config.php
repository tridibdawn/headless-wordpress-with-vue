<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'uplers_appeal_assessments' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '3zqTqhYS5Hjm:hq#BmHL[U9-]FMh2 ]`Z,]%Q,A8}98DoMzdku)`8@_xb!Q&sxH_' );
define( 'SECURE_AUTH_KEY',  'M ,q;Uvvrl;~x;v=~*fTuEU)UP(1u2NLAw`=jorPM:VNKos|L!ULjz6gJI)nueuE' );
define( 'LOGGED_IN_KEY',    'TzA{<GB&I=%mg6!3nyGpFKq/@V}n $HLzy_t1H>CIUoGAleB]l,xySy{lm?JEZ61' );
define( 'NONCE_KEY',        'wb=G4jl]^Ox@d{:D`.XZ,TPlv{T#YOwnXS+AhJG${mJ4/R&SMzxVhj~Z%geUc;n4' );
define( 'AUTH_SALT',        'W}<*Dh$hwi;ckj[iFt3+fPEI&kr(&P-S_XFL$n42Cvk}8Hse)3G)MC25KN;Dl/?l' );
define( 'SECURE_AUTH_SALT', 'DFf&L<g/?;T?a1tNn)w6NBT2;Ago!!evS`ngR0B#[ f1|IS/PIxf{OB{=xXOu&_?' );
define( 'LOGGED_IN_SALT',   'YnzI}@Jbp#T9dX<w3O&FvsY>kOTXdy;c&IK$( xowjz0VxB~mY_q:0U5[M+|>$ym' );
define( 'NONCE_SALT',       'N+u}WRI1VWPlOD],~A! BBDI%%U:,YD^W~Pw`ubU&q{;8,~oY6Q,]_/h)X-8QVh`' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
